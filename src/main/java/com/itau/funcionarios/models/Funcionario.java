package com.itau.funcionarios.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Entity
@Table(name="employees")
public class Funcionario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="employee_id")
	private int id;
	@NotBlank
	@Column(name="first_name")
	private String nome;
	@NotBlank
	@Column(name="last_name")
	private String sobrenome;
	@NotBlank
	@Column(name="email")
	private String email;
	@NotBlank
	@Column(name="phone_number")
	private String telefone;
	@NotNull
	@Column(name="hire_date")
	private LocalDate contratacao;
	@Min(1000)
	@Column(name="salary")
	private double salario;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="job_id")
	private Cargo cargo;
	@JoinColumn(name="manager_id")
	@ManyToOne
	private Funcionario gerente;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSobrenome() {
		return sobrenome;
	}
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public LocalDate getContratacao() {
		return contratacao;
	}
	public void setContratacao(LocalDate contratacao) {
		this.contratacao = contratacao;
	}
	public double getSalario() {
		return salario;
	}
	public void setSalario(double salario) {
		this.salario = salario;
	}
	public Cargo getCargo() {
		return cargo;
	}
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	public Funcionario getGerente() {
		return gerente;
	}
	public void setGerente(Funcionario gerente) {
		this.gerente = gerente;
	}
}
