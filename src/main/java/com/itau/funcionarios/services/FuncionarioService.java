package com.itau.funcionarios.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.funcionarios.models.Funcionario;
import com.itau.funcionarios.models.Localizacao;
import com.itau.funcionarios.repositories.FuncionarioRepository;
import com.itau.funcionarios.repositories.LocalizacaoRepository;

@Service
public class FuncionarioService {
	@Autowired
	FuncionarioRepository funcionarioRepository;

	public Iterable<Funcionario> buscarTodos() {
		return funcionarioRepository.findAll();
	}

	public void inserir(Funcionario funcionario) {
		funcionarioRepository.save(funcionario);
	}

	public boolean substituir(int id, Funcionario funcionario) {
		Optional<Funcionario> funcionarioOptional = funcionarioRepository.findById(id);

		funcionario.setId(id);

		if (funcionarioOptional.isPresent()) {
			funcionarioRepository.save(funcionario);
			return true;
		}

		return false;
	}

	public boolean atualizar(int id, Funcionario funcionario) {
		Optional<Funcionario> funcionarioOptional = funcionarioRepository.findById(id);

		if (funcionarioOptional.isPresent()) {
			funcionario = mesclarAtributos(funcionario, funcionarioOptional.get());

			funcionarioRepository.save(funcionario);
			return true;
		}

		return false;
	}

	public boolean remover(int id) {
		Optional<Funcionario> funcionarioOptional = funcionarioRepository.findById(id);

		if (funcionarioOptional.isPresent()) {
			funcionarioRepository.delete(funcionarioOptional.get());
			return true;
		}

		return false;
	}

	private Funcionario mesclarAtributos(Funcionario novo, Funcionario antigo) {
		if (novo.getNome() != null && !novo.getNome().isEmpty()) {
			antigo.setNome(novo.getNome());
		}

		if (novo.getEmail() != null && !novo.getEmail().isEmpty()) {
			antigo.setEmail(novo.getEmail());
		}

		// ... fazer para outros atributos atualizáveis

		return antigo;
	}
}
